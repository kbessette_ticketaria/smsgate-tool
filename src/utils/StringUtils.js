module.exports = {
  RemoveAccents(text) {
    if (!text) {
      return text;
    }
    return text.normalize("NFD").replace(/[\u0080-\u036f]/g, "");
  }
};
