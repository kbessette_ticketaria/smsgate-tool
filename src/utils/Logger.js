const config = require("../../config/default.json");

module.exports = (text, object) => {
  if (config.verbose === true) {
    console.log(text, object);
  }
};
