const serialportgsm = require("serialport-gsm");
const smsgateApi = require("./smsgateApi");
const SlackNotifier = require("./SlackNotifier");
const moment = require("moment-timezone");
const StringUtils = require("./utils/StringUtils");
const Logger = require("./utils/Logger");
const phoneList = require("../config/phones.json");

class SerialModem {
  constructor(port) {
    this.port = port;
    this.phoneNumber = "UNKNOWN";
    this.operator = " 0";
    this.isOpen = false;
    this.isListening = false;
    this.options = {
      baudRate: 115200,
      dataBits: 8,
      stopBits: 1,
      parity: "none",
      rtscts: false,
      xon: false,
      xoff: false,
      xany: false,
      autoDeleteOnReceive: false,
      enableConcatenation: true,
      incomingCallIndication: true,
      incomingSMSIndication: true,
      pin: "",
      customInitCommand: "",
      timeout: 10000,
      logger: undefined
    };
    this.gsmmodem = serialportgsm.Modem();
    this.initOnOpen();
  }

  initOnOpen() {
    this.gsmmodem.on("open", (open, errOpen) => {
      if (errOpen) {
        console.log("errOpen", errOpen);
        smsgateApi.sendModemError(this.port, errOpen);
      }
      Logger(`${this.comPort()} opened`);
    });
    this.gsmmodem.on("onNewMessage", async (data, errReceived) => {
      if (errReceived) {
        console.error("errReceived", errReceived);
      } else {
        await this.handleSmsReceived(data);
      }
    });
    this.initOnClose();
    this.initOnError();
  }

  async initializeModem() {
    return new Promise((res, rej) => {
      this.gsmmodem.initializeModem(
        (init, errInit) => {
          if (errInit) {
            if (!errInit.message.includes("timeout:")) {
              console.log(
                `error InitializeModem com:${this.port} phone:${this.phoneNumber}`,
                errInit
              );
              smsgateApi.sendModemError(this.port, errInit);
              rej(errInit);
            } else {
              rej();
            }
          } else {
            res(init);
          }
        },
        false,
        this.options.timeout
      );
    });
  }

  async setMode() {
    return new Promise((res, rej) => {
      this.gsmmodem.setModemMode((msg, errMode) => {
        if (errMode) {
          console.log("errMode", errMode);
          smsgateApi.sendModemError(this.port, errMode);
          rej(errMode);
        } else {
          this.isListening = true;
          res();
        }
      }, "PDU");
    });
  }

  async getPhoneNumber() {
    return new Promise((res, rej) => {
      this.gsmmodem.getOwnNumber(async (number, errNumber) => {
        if (errNumber) {
          rej(errNumber);
        } else if (number.status !== "ERROR") {
          console.log(
            `COM${this.port}, number:${number.data.number} Phone number fetched`
          );
          this.phoneNumber = number.data.number;
          res();
        } else {
          rej(number);
        }
      });
    });
  }

  retryGetPhoneNumber() {
    setTimeout(async () => {
      try {
        await this.getPhoneNumber();
      } catch (err) {}
    }, this.options.timeout);
  }

  async setPhoneNumber() {
    return new Promise((res, rej) => {
      if (phoneList) {
        const foundPhone = phoneList.find(p => p.comport === this.port);
        if (foundPhone) {
          console.log(`COM${this.port}, foundPhone`, foundPhone);
          this.gsmmodem.selectPhonebookStorage("ON", phoneBook => {
            console.log(`COM${this.port}, phoneBook`, phoneBook);
            this.gsmmodem.writeToPhonebook(
              foundPhone.phoneNumber,
              "ON",
              (result, error) => {
                if (error) {
                  console.log(`COM${this.port}, error`, error);
                } else {
                  console.log(`COM${this.port}, setPhoneNumber result`, result);
                }
                res();
              }
            );
          });
        } else {
          res();
        }
      } else {
        res();
      }
    });
  }

  async getOperator() {
    return new Promise((res, rej) => {
      this.gsmmodem.executeCommand(
        "AT+COPS?",
        (result, err) => {
          if (err) {
            console.log(`getOperator callback Error - ${err}`);
            console.log(`==================`);
            smsgateApi.sendModemError(this.port, err);
            rej(err);
          } else if (result.status === "ERROR") {
            console.log("getOperator callback Error", result);
            smsgateApi.sendModemError(this.port, result);
            rej(result);
          } else {
            this.operator = result.data.result;
            res();
          }
        },
        false,
        this.options.timeout
      );
    });
  }

  retryGetOperator() {
    setTimeout(async () => {
      try {
        await this.getOperator();
      } catch (err) {}
    }, this.options.timeout);
  }

  initOnError() {
    this.gsmmodem.on("error", result => {
      this.isOpen = !!this.gsmmodem.port
        ? this.gsmmodem.port.isOpen
        : this.isOpen;
      smsgateApi.sendModemError(this.port, result);
      //console.error(`${this.comPort()}, number:${this.phoneNumber} GSM MODEM ERROR result`, result);
    });
  }

  initOnClose() {
    this.gsmmodem.on("close", msg => {
      if (this.isListening) {
        console.log(`${this.comPort()} Closed`);
      }
      this.phoneNumber = "UNKNOWN";
      this.operator = " 0";
      this.isListening = false;
      this.isOpen = false;
      this.retryStart();
    });
  }

  getSignal() {
    return new Promise((res, rej) => {
      this.gsmmodem.getNetworkSignal(async (signal, errSignal) => {
        if (errSignal) {
          rej(errSignal);
        } else if (signal.status !== "ERROR") {
          console.log(
            `COM${this.port}, number:${
              this.phoneNumber
            }, signal:${JSON.stringify(signal.data)}`
          );
          this.signal = signal.data;
          res();
        } else {
          rej(signal);
        }
      });
    });
  }

  comPort() {
    return `COM${this.port}`;
  }

  async startModem() {
    try {
      await this.open();
      try {
        await this.start();
        try {
          const amount = await this.checkSimMemory();
          const smsList = await this.getSimInbox(amount);
          this.sendSMSList(smsList, this.port);
        } catch (error) {
          Logger(
            `${this.comPort()} error getting messages from sim card`,
            error
          );
          smsgateApi.sendModemError(this.port, error);
        }
        try {
          this.getSignal();
        } catch (error) {
          console.log("error GetSignal", error);
        }
        await this.setMode();
      } catch (startError) {
        if (startError) {
          console.error(`${this.comPort()} start error`, startError);
        }
        try {
          await this.close();
        } catch (errorClose) {}
      }
    } catch (openError) {
      Logger(`${this.comPort()} openError`, openError);
      this.retryStart();
    }
  }

  async open() {
    return new Promise(async (res, rej) => {
      try {
        if (this.isOpen) {
          res();
        } else {
          this.gsmmodem.open(this.comPort(), this.options, (error, msg) => {
            if (error) {
              if (
                !error.message.includes("Error: Opening COM") &&
                !error.message.includes("File not found")
              ) {
                console.log(`${this.comPort()} error this.modem.open`, error);
                smsgateApi.sendModemError(this.port, error);
                rej(error);
              } else {
                rej();
              }
            } else {
              this.isOpen = true;
              res(true);
            }
          });
        }
      } catch (error) {
        console.error(`${this.comPort()} Error while Open`, error);
        rej(error);
      }
    });
  }

  async start() {
    return new Promise(async (res, rej) => {
      try {
        await this.initializeModem();
        try {
          await this.setPhoneNumber();
          await this.getPhoneNumber();
        } catch (errIgnored) {
          this.retryGetPhoneNumber();
        }
        try {
          await this.getOperator();
        } catch (error) {
          this.retryGetOperator();
        }
      } catch (err) {
        return rej(err);
      }
      res();
    });
  }

  retryStart() {
    if (!this.timeoutStart) {
      this.timeoutStart = setTimeout(async () => {
        this.timeoutStart = null;
        await this.startModem();
      }, this.options.timeout);
    }
  }

  async close() {
    return new Promise((res, rej) => {
      if (this.isOpen) {
        this.gsmmodem.close((error, result) => {
          if (error) {
            console.log(`${this.comPort()} close error`, error);
            rej(error);
          } else {
            res();
          }
        });
      } else {
        this.isOpen = false;
        this.retryStart();
        res();
      }
    });
  }

  async getSimInbox(inboxLength) {
    return new Promise((res, rej) => {
      let resultData = null;
      const commandParser = this.gsmmodem.executeCommand(
        'AT+CMGL="ALL"',
        (result, err) => {
          if (err) {
            console.log(`Error - ${err}`);
            console.log(`==================`);
            smsgateApi.sendModemError(this.port, err);
            rej(err);
          } else if (result.status === "ERROR") {
            smsgateApi.sendModemError(this.port, result);
            rej(result);
          } else {
            res(result);
          }
        },
        false,
        this.options.timeout
      );
      commandParser.logic = dataLine => {
        if (!resultData) {
          resultData = {
            status: "success",
            request: "getSimInbox",
            data: []
          };
        }

        if (dataLine.includes("+CMGL:")) {
          const splitted = dataLine.split(",");
          resultData.data.push({
            to: this.phoneNumber,
            from: splitted[2].replace(/\"/g, ""),
            date: splitted[4].replace(/\"/g, ""),
            time: splitted[5].substr(0, 8),
            index: parseInt(splitted[0].replace("+CMGL: ", ""), 10),
            text: ""
          });
        } else if (dataLine === ">" || dataLine === "OK") {
          if (inboxLength === 0 || resultData.data.length > 0) {
            return {
              resultData,
              returnResult: true
            };
          }
        } else if (dataLine.includes("ERROR")) {
          resultData = {
            status: "ERROR",
            request: "getSimInbox",
            data: `Cannot Get Sim Inbox ${dataLine}`
          };
          return {
            resultData,
            returnResult: true
          };
        } else if (dataLine.length > 1 && !dataLine.includes("+CMGL")) {
          try {
            resultData.data[resultData.data.length - 1].text += `${dataLine} `;
          } catch (err) {
            console.log(`dataLine not added to msg: "${dataLine}`);
          }
        }
      };
    });
  }

  async checkSimMemory() {
    return new Promise((res, rej) => {
      this.gsmmodem.checkSimMemory(async (result, err) => {
        if (err) {
          console.log(
            `${this.comPort()}, number:${
              this.phoneNumber
            } Failed to get SimMemory\n${err}`
          );
          smsgateApi.sendModemError(this.port, result);
          rej(err);
        } else {
          //result = {"status":"success","request":"checkSimMemory","data":{"used":0,"total":40}}
          if (result.status === "success") {
            console.log(
              `${this.comPort()}, number:${
                this.phoneNumber
              } Memory Result: ${JSON.stringify(result.data)}`
            );
          } else {
            console.error(
              `${this.comPort()}, number:${
                this.phoneNumber
              } Memory Result: ${JSON.stringify(result)}`
            );
          }
          if (result.data && result.data.used > 0) {
            res(result.data.used);
          } else {
            res(0);
          }
        }
      });
    });
  }

  async handleSmsReceived(data) {
    try {
      const phoneNumber = this.phoneNumber || "UNKNOWN";
      const dateTimeSent = moment
        .tz(data.dateTimeSend, "UTC")
        .tz("America/Toronto")
        .format("YYYY-MM-DD HH:mm:ss");

      const sms = {
        to: phoneNumber,
        from: data.sender,
        text: data.message,
        index: data.index,
        dateTimeSent,
        comport: this.port
      };
      try {
        await smsgateApi.sendSms(sms);
        await this.deleteSms(sms);
      } catch (error) {
        console.log(
          `${this.comPort()}, number:${
            this.phoneNumber
          } Error while saving sms to database or delete SMS from sim`,
          error
        );
      }
    } catch (error) {
      console.log("error sendSMS", error);
    }
  }

  async deleteSms(sms) {
    return new Promise((res, rej) => {
      this.gsmmodem.deleteSimMessages(
        sms.index,
        (result, error) => {
          if (error) {
            rej(error);
          } else {
            res();
          }
        },
        null,
        500
      );
    });
  }

  async sendSMSList(smsList, port) {
    for (let index = 0; index < smsList.data.length; index++) {
      const sms = smsList.data[index];
      try {
        const dateTimeSent = moment
          .tz(`${sms.date} ${sms.time}`, "YY/MM/DD HH:mm:ss", "UTC")
          .format("YYYY-MM-DD HH:mm:ss");
        sms.dateTimeSent = dateTimeSent;
        sms.comport = this.port;
        await smsgateApi.sendSms(sms);
        await this.deleteSms(sms);
      } catch (error) {
        console.log("error", error);
        smsgateApi.sendModemError(port, error);
      }
    }
  }
}

module.exports = SerialModem;
