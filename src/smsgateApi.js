const config = require("../config/default.json");
const axios = require("axios");
const StringUtils = require("./utils/StringUtils");
const moment = require("moment-timezone");

module.exports = {
  async sendSms({ to, from, text, dateTimeSent, comport }) {
    const url = `${config.smsgateUrl}/api/sms/`;
    const modem = config.modem;
    const message = StringUtils.RemoveAccents(text);
    const body = {
      modem,
      to,
      from,
      message,
      dateTimeSent,
      comport
    };
    const dateTimeReceived = moment.tz("America/Toronto")
    .format("DD HH:mm:ss");
    console.log(`S:${dateTimeSent} - R:${dateTimeReceived} - ${comport} - ${to} - ${message}`);
    try {
      await axios.post(url, body);
    } catch (error) {
      if (!error.response || ![200, 409].includes(error.response.status)) {
        console.error("=============");
        console.log("body", body);
        console.error("=============");
        console.error("sendSms", error.response ? error.response.data : await error.toJSON());
        console.error("=============");
      }
    }
  },

  async updateModems(modems) {
    const url = `${config.smsgateUrl}/api/modem/all`;
    const modemId = config.modem;
    const body = {
      modemId,
      modems: modems.map(m => {
        return {
          port: m.port,
          phoneNumber: m.phoneNumber,
          isListening: m.isListening,
          isOpen: m.isOpen,
          operator: m.operator,
        };
      })
    };
    try {
      await axios.put(url, body);
    } catch (error) {
      if (!error.response || error.response.status !== 200) {
        console.error("=============");
        console.log("body", body);
        console.error("=============");
        console.error("updateModems", error.response ? error.response.data : error);
        console.error("=============");
        console.error("updateModems json", error.toJSON());
        console.error("=============");

      }
    }
  },

  async updateModem({ port, phoneNumber, isListening, isOpen, operator }) {
    const url = `${config.smsgateUrl}/api/modem/`;
    const modem = config.modem;
    const body = { modem, port, phoneNumber, isOpen, isListening, operator };
    try {
      await axios.put(url, body);
    } catch (error) {
      if (!error.response || error.response.status !== 200) {
        console.error("=============");
        console.log("body", body);
        console.error("=============");
        console.error("updateModem", error.response ? error.response.data : error);
        console.error("=============");
      }
    }
  },

  async sendModemError(port, error) {
    const url = `${config.smsgateUrl}/api/modem/error`;
    const modem = config.modem;
    const stackTrace = new Error().stack;
    const body = { modem, port, error: JSON.stringify(error), stackTrace };
    try {
      await axios.post(url, body);
    } catch (error) {
      if (!error.response || error.response.status !== 200) {
        console.error("=============");
        console.log("body", body);
        console.error("=============");
        console.error("sendModemError", error.response ? error.response.data : error);
        console.error("=============");
      }
    }
  }
};
