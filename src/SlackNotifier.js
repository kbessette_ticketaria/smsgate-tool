const config = require("../config/default.json");
const axios = require("axios");

module.exports = async (to, message) => {
  const url = config.slackWebHook;

  const requestConfig = JSON.stringify({
    attachments: [
      {
        title: to || "UNKNOWN",
        text: message || ""
      }
    ]
  });

  try {
    await axios.post(url, requestConfig);
  } catch (error) {
    console.error("=============");
    console.error(error);
    console.error("=============");
    console.error(error.response);
    console.error("=============");
  }
};
