const SerialModem = require("./SerialModem");
const smsgateApi = require("./smsgateApi");
const config = require("../config/default.json");

class ModemServer {
  constructor() {
    this.modems = [];
  }

  startAll() {
    this.modems.forEach(modem => {
      modem.startModem();
    });
  }

  createAll() {
    for (let index = 2; index < 270; index++) {
      this.create(index);
    }
  }

  create(port) {
    const modem = new SerialModem(port);
    this.modems.push(modem);
  }

  async sendServerStatus() {
    if (this.modems && this.modems.length > 0) {
      const openModem = this.modems.reduce((acc, current) => {
        if (current.isOpen) {
          acc.push(current);
        }
        return acc;
      }, []);
      await smsgateApi.updateModems(openModem);
    }
  }

  updateServerStatus() {
    setInterval(async () => {
      try {
        await this.sendServerStatus();
      } catch (error) {
        console.log("error", error);
      }
    }, config.refreshStatusDelai);
  }
}

module.exports = ModemServer;
