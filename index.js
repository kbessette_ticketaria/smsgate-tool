const config = require("./config/default");
const Sentry = require("@sentry/node");
const ModemServer = require("./src/ModemServer");

let server;
const startServer = async () => {
  try {
    server = new ModemServer();
    server.createAll();
    server.startAll();
    server.updateServerStatus();
  } catch (err) {
    console.log('err', err);
  }
};

(async () => {
  try {
    if (!config.debug) {
      Sentry.init({ dsn: config.sentryDSN });
    }
    startServer();
  } catch (error) {
    console.error(error);
    if (!config.debug) {
      try {
        Sentry.configureScope(scope => {
          scope.setExtra("MODEM", config.modem);
        });
      } catch (err) {
        Sentry.captureException(err);
      }
      Sentry.captureException(error);
    }
  }
})();
